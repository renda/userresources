package uk.co.photobox.ppagnoniTest.utility;

import org.junit.Test;

import junit.framework.Assert;

public class DigestTest {

	@Test
	public void testDigestSHA1() {
		String s = "password";
		String expectedHash = "5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8";
		String hash = Digest.digestSHA1(s);
		Assert.assertNotNull(hash);
		Assert.assertNotSame(hash, s);
		Assert.assertEquals(hash, expectedHash);
	}
	
	@Test(expected = NullPointerException.class)
	public void testDigestSHA1Null() {
		String s = null;
		@SuppressWarnings("unused")
		String hash = Digest.digestSHA1(s);
	}
	
	@Test
	public void testDigestSHA1Empty() {
		String s = "";
		String expectedHash = "da39a3ee5e6b4b0d3255bfef95601890afd80709";
		String hash = Digest.digestSHA1(s);
		Assert.assertNotNull(hash);
		Assert.assertNotSame(hash, s);
		Assert.assertEquals(hash, expectedHash);
	}

}
