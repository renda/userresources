package uk.co.photobox.ppagnoniTest.services;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import junit.framework.Assert;

import org.junit.BeforeClass;
import org.junit.Test;

import uk.co.photobox.ppagnoniTest.domain.model.User;
import uk.co.photobox.ppagnoniTest.domain.type.UserTitle;
import uk.co.photobox.ppagnoniTest.domain.type.UserType;

public class UserServiceTest {

	private static UserService userService; 
	
	@BeforeClass
	public static void setUp(){
		List<User> users = new ArrayList<User>();
		User user = getDummyUser(1);
		users.add(user);
		users.add(getDummyUser(2));
		userService = mock(UserService.class);
		when(userService.findAllUsers()).thenReturn(users);
		when(userService.findUserById(org.mockito.Mockito.anyString())).thenReturn(user);
		when(userService.findUserByType(org.mockito.Mockito.any(UserType.class))).thenReturn(users);
		when(userService.findUserByCredential(org.mockito.Mockito.anyString(), org.mockito.Mockito.anyString())).thenReturn(user);
		
	}
	
	@Test
	public void testFindAllUsers() {
		List<User> users = userService.findAllUsers();
		Assert.assertNotNull(users);
		Assert.assertEquals(2, users.size());
	}

	@Test
	public void testFindUserById() {
		User user = userService.findUserById("email@user1");
		Assert.assertNotNull(user);
		Assert.assertEquals(user.getFirstName(), "User FN" + 1);
	}


	@Test
	public void testFindUserByType() {
		List<User> users = userService.findUserByType(UserType.SUBSCRIBER);
		Assert.assertNotNull(users);
		Assert.assertEquals(2, users.size());
	}

	@Test
	public void testFindUserByCredential() {
		User user = userService.findUserByCredential("email@user1","pwd");
		Assert.assertNotNull(user);
		Assert.assertEquals(user.getFirstName(), "User FN" + 1);
	}
	
	private static User getDummyUser(int n){
		User user = new User();
		user.setUserType(UserType.SUBSCRIBER);
		user.setFirstName("User FN" + n);
		user.setLastName("User LN" + n);
		user.setUserTitle(UserTitle.MISS);
		user.setDob(new Date());
		user.setEmailAddress("email@user"+ n);
		user.setPassword("pwd" + n);
		user.setHomeAddress("HomeAddress" + n);
		user.setBillingAddress("BillindAddr " + n);
		return user;
	}

}
