package uk.co.photobox.ppagnoniTest.api;

import java.util.List;

import javax.ws.rs.core.Application;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

import org.eclipse.jetty.http.HttpStatus;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.TestProperties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import junit.framework.Assert;
import uk.co.photobox.ppagnoniTest.domain.model.User;

public class UserResourcesTest extends JerseyTest{

	private static final Logger logger = LoggerFactory.getLogger(UserResourcesTest.class);
	
	
	@Before
	public void setUpChild() { }

	@After
	public void tearDownChild() { }

	@Test
	public void testGetAllUser() {
		logger.debug("Test getAllUsers");
        Response response = target("/users").request().get();

        Assert.assertEquals(HttpStatus.OK_200, response.getStatus());
        List<User> users = response.readEntity(new GenericType<List<User>>()
                {});
        Assert.assertNotNull(users);
        Assert.assertEquals(4, users.size());
	}

	@Test
	public void testGetUserById() {
        Response response = target("/users" + "/test@test.com").request().get();

        Assert.assertEquals(HttpStatus.OK_200, response.getStatus());
        User user = response.readEntity(new GenericType<User>()
                {});
        Assert.assertNull(user);
        
        response = target("/users" + "/email@admin01.com").request().get();

        Assert.assertEquals(HttpStatus.OK_200, response.getStatus());
        user = response.readEntity(new GenericType<User>()
                {});
        Assert.assertNotNull(user);
        Assert.assertEquals("Admin01 FN", user.getFirstName());

	}

	//TODO other methods to test

    @Override
    protected Application configure()
    {
        // Find first available port for the servlet container
        forceSet(TestProperties.CONTAINER_PORT, "0");

        return new ResourceConfig(UserResources.class);
    }

}
