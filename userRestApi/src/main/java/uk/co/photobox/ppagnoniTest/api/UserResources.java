package uk.co.photobox.ppagnoniTest.api;

import java.util.Arrays;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import uk.co.photobox.ppagnoniTest.domain.model.User;
import uk.co.photobox.ppagnoniTest.domain.type.UserType;
import uk.co.photobox.ppagnoniTest.services.UserService;
import uk.co.photobox.ppagnoniTest.services.UserServiceImpl;

@Path("/users")
@Api(value = "/users", description = "User Service")
public class UserResources {
	
    private static final Logger logger = LoggerFactory.getLogger(UserResources.class);

    private UserService userService = new UserServiceImpl();
    
	
	// all user
	@GET
	@ApiOperation(value = "Get all the users",
		    response = List.class,
		    responseContainer = "List")
    @ApiResponses({
        @ApiResponse(code = 500, message = "for internal errors")
        })
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllUser() {
		List<User> allUser = userService.findAllUsers();
		GenericEntity<List<User>> entity = new GenericEntity<List<User>>(allUser) { };
		logger.debug("Get Users found: {} users", entity.getEntity().size());
		return Response.ok( entity ).build();
	}
	
	// user by id=emailAddress
	@GET
	@Path("/{emailAddress}")
	@ApiOperation(value = "Get user by email",
    response = User.class)
	@ApiResponses({ 
		@ApiResponse(code = 404, message = "in case the email is empty or null"),
		@ApiResponse(code = 500, message = "for internal errors")
		}
	)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUserById(@PathParam("emailAddress") @NotNull String emailAddress) {
		if(StringUtils.isEmpty(emailAddress)){
			throw new BadRequestException("The email address is empty or null");
		}
		User user = userService.findUserById(emailAddress);
		logger.debug("Get User: {}", emailAddress);
		return Response.ok(user).build();
	}
	
	// return userList with a specified userType
	// ex.: /userCredential?emailAddress=...&password=...
	@GET
	@Path("/userCredential")
	@ApiOperation(value = "Get user by email and password",
    response = User.class)
	@ApiResponses({ 
		@ApiResponse(code = 404, message = "in case the email or password are empty or null"),
		@ApiResponse(code = 500, message = "for internal errors")
		}
	)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUserByCredential(@Context UriInfo info) {
		String emailAddress = info.getQueryParameters().getFirst("emailAddress");
		String password = info.getQueryParameters().getFirst("password");
		if(StringUtils.isEmpty(emailAddress) || StringUtils.isEmpty(password) ){
			throw new BadRequestException("The email address or password are empty or null");
		}
			User user =
					userService.findUserByCredential(emailAddress, password);
			return Response.ok(user).build();
	}
	
	// return userTypeList
	@GET
	@Path("/userTypeList")
	@ApiOperation(value = "Get users types",
    response = List.class,
    responseContainer = "List")
	@ApiResponses({ 
		@ApiResponse(code = 500, message = "for internal errors")
		}
	)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUser() {
		List<UserType> userTypeList = Arrays.asList(UserType.values());
		GenericEntity<List<UserType>> entity = new GenericEntity<List<UserType>>(userTypeList) { };
		return Response.ok(entity).build();
	}
	
	// return userList with a specified userType
	// ex.: /query?userType=ADMINISTRATOR
	@GET
	@Path("/query")
	@ApiOperation(value = "Get users by type or empty list in case the userType is not correct",
    response = List.class,
    responseContainer = "List"
	)
	@ApiResponses({ 
		@ApiResponse(code = 500, message = "for internal errors")
		}
	)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUserByType(@Context UriInfo info) {
		String userType = info.getQueryParameters().getFirst("userType");
		try {
			List<User> userByType =
					userService.findUserByType(UserType.valueOf(userType));
			GenericEntity<List<User>> entity = new GenericEntity<List<User>>(userByType) { };
			return Response.ok(entity).build();
		} catch (IllegalArgumentException e){
			// empty list if userType is not correct
			return Response.ok(null).build();
		}
	}

	// create new user
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces({MediaType.TEXT_PLAIN})
	@ApiOperation(value = "Create the user",
    response = String.class)
	@ApiResponses({ 
		@ApiResponse(code = 500, message = "for internal errors")
		}
	)
	
//	@ValidateOnExecution
//	public Response crateUser(@Valid User user) {
	public Response createUser(@NotNull User user) {
		String outcome = userService.saveUser(user);
		
		// insert ok
		if (user.getEmailAddress().equals(outcome)) {
			logger.debug("POST User created: {}", user.getEmailAddress());
			return Response.status(Response.Status.CREATED).entity(outcome).build();
		} else {
			// existing user
			logger.debug("User already exist: {}", user.getEmailAddress());
			return Response.ok(null).build();
		}
	}
	
	// update user
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Update the user")
	@ApiResponses({ 
		@ApiResponse(code = 500, message = "for internal errors")
		}
	)
	public void updateUser(@NotNull User user) {
		userService.updateUser(user);
		logger.debug("PUT User updated: {}", user.getEmailAddress());
	}
	
	// delete user with specified id=emailAddress
	@DELETE
	@Path("/{emailAddress}")
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Delete the user")
	@ApiResponses({ 
		@ApiResponse(code = 500, message = "for internal errors")
		}
	)
	public void deleteUser(@PathParam("emailAddress") @NotNull String emailAddress) {
		userService.deleteUser(emailAddress);
		logger.debug("DELETE User deleted: {}", emailAddress);
	}
}
