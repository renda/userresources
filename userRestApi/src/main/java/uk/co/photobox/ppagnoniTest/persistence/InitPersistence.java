package uk.co.photobox.ppagnoniTest.persistence;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import uk.co.photobox.ppagnoniTest.domain.model.User;
import uk.co.photobox.ppagnoniTest.domain.type.UserTitle;
import uk.co.photobox.ppagnoniTest.domain.type.UserType;

public class InitPersistence {

	
	public Map<String, User> init () {
	
		User userSub01 = new User();
		userSub01.setUserType(UserType.SUBSCRIBER);
		userSub01.setFirstName("User01 FN");
		userSub01.setLastName("User01 LN");
		userSub01.setUserTitle(UserTitle.MISS);
		userSub01.setDob(new Date());
		userSub01.setEmailAddress("email@user01.com");
		userSub01.setPassword("b501ea2e412e290f6782c7f7e00d105769b81e76");
//		userSub01.setPassword(Digest.digestSHA1("pwd01"));
		userSub01.setHomeAddress("HomeAddress01");
		userSub01.setBillingAddress("BillindAddr01");
		
		User userSub02 = new User();
		userSub02.setUserType(UserType.SUBSCRIBER);
		userSub02.setFirstName("User02 FN");
		userSub02.setLastName("User 02 LN");
		userSub02.setUserTitle(UserTitle.MISS);
		userSub02.setDob(new Date());
		userSub02.setEmailAddress("email@user02.com");
		userSub02.setPassword("b501ea2e412e290f6782c7f7e00d105769b81e76");
//		userSub01.setPassword(Digest.digestSHA1("pwd01"));
		userSub02.setHomeAddress("HomeAddress02");
		userSub02.setBillingAddress("BillindAddr02");
		
		User userAdmin01 = new User();
		userAdmin01.setUserType(UserType.ADMINISTRATOR);
		userAdmin01.setFirstName("Admin01 FN");
		userAdmin01.setLastName("Admin01 LN");
		userAdmin01.setUserTitle(UserTitle.MISS);
		userAdmin01.setDob(new Date());
		userAdmin01.setEmailAddress("email@admin01.com");
		userAdmin01.setPassword("b501ea2e412e290f6782c7f7e00d105769b81e76");
//		userSub01.setPassword(Digest.digestSHA1("pwd01"));
//		userAdmin01.setHomeAddress("HomeAddress01admin");
//		userAdmin01.setBillingAddress("BillindAddr01admin");
		
		User userSuperUser = new User();
		userSuperUser.setUserType(UserType.SUPER_USER);
		userSuperUser.setFirstName("SuperUser01 FN");
		userSuperUser.setLastName("SuperUser 01 LN");
		userSuperUser.setUserTitle(UserTitle.MISS);
		userSuperUser.setDob(new Date());
		userSuperUser.setEmailAddress("email@superuser01.com");
		userSuperUser.setPassword("b501ea2e412e290f6782c7f7e00d105769b81e76");
//		userSub01.setPassword(Digest.digestSHA1("pwd01"));
//		userSuperUser.setHomeAddress("HomeAddress01SuperU");
//		userSuperUser.setBillingAddress("BillindAddr01SuperU");
	
		Map<String, User> um = new HashMap<>();
		um.put(userSub01.getEmailAddress(), userSub01);
		um.put(userSub02.getEmailAddress(), userSub02);
		um.put(userAdmin01.getEmailAddress(), userAdmin01);
		um.put(userSuperUser.getEmailAddress(), userSuperUser);
		
		return um;
	}
		
}
