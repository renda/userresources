package uk.co.photobox.ppagnoniTest.domain.type;

public enum UserTitle {
		MISS("Miss", 0), MR("Mr.", 1);

		private final String name;
		private final int value;

		private UserTitle(String name, int value) {
			this.name = name;
			this.value = value;
		}

		@Override
		public String toString() {
			return this.name;
		}

		public String getName() {
			return name;
		}

		public int getValue() {
			return value;
		}
}
