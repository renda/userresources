package uk.co.photobox.ppagnoniTest.domain.model;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import uk.co.photobox.ppagnoniTest.domain.type.UserTitle;
import uk.co.photobox.ppagnoniTest.domain.type.UserType;

/**
 * A plain User resource
 * @author  Pierpaolo Pagnoni
 */

@XmlRootElement
@ApiModel
public class User {

    @ApiModelProperty(value = "Type of the user", 
    		allowableValues = "Subscriber, Administrator, SuperUSer", 
    		example = "Subscriber",
    		dataType = "Object",
    		required = true)
	private UserType userType;
    
    @ApiModelProperty(value = "Firstname")
	private String firstName;
//	@NotNull
    @ApiModelProperty(value = "Firstname")
	private String lastName;
    
    @ApiModelProperty(value = "User Title", 
    		allowableValues = "Mr., Miss", 
    		example = "Mr.",
    		dataType = "Object",
    		required = false)
	private UserTitle userTitle;
    
    @ApiModelProperty(value = "Date of birth")
	private Date dob;
    
    @ApiModelProperty(value = "Email address")
	private String emailAddress;
    
    @ApiModelProperty(value = "Password")
	private String password;
    
    @ApiModelProperty(value = "Home Address")
	private String homeAddress;
    
    @ApiModelProperty(value = "Billing Address")
	private String billingAddress;

	public UserType getUserType() {
		return userType;
	}

	public void setUserType(UserType userType) {
		this.userType = userType;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public UserTitle getUserTitle() {
		return userTitle;
	}

	public void setUserTitle(UserTitle userTitle) {
		this.userTitle = userTitle;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password; //Digest.digestSHA1(password);
	}

	public String getHomeAddress() {
		return homeAddress;
	}

	public void setHomeAddress(String homeAddress) {
		this.homeAddress = homeAddress;
	}

	public String getBillingAddress() {
		return billingAddress;
	}

	public void setBillingAddress(String billingAddress) {
		this.billingAddress = billingAddress;
	}
}
