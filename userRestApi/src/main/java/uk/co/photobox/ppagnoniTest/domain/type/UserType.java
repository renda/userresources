package uk.co.photobox.ppagnoniTest.domain.type;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public enum UserType {
		SUBSCRIBER("Subscriber", 0), ADMINISTRATOR("Administrator", 1), SUPER_USER("SuperUSer", 2);

		private final String name;
		private final int value;

		private UserType(String name, int value) {
			this.name = name;
			this.value = value;
		}

		@Override
		public String toString() {
			return this.name;
		}

		public String getName() {
			return name;
		}

		public int getValue() {
			return value;
		}
}
