package uk.co.photobox.ppagnoniTest.persistence;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import uk.co.photobox.ppagnoniTest.domain.model.User;
import uk.co.photobox.ppagnoniTest.domain.type.UserType;
import uk.co.photobox.ppagnoniTest.utility.Digest;

public class UserPersistence {

	private static Map<String, User> userMap = new InitPersistence().init();
	
	
	public List<User> findAllUser(){
		List<User> userList = new ArrayList<>(userMap.values());
		return userList;		
	}
	
	public User findUserById(String userEmail) {
		return userMap.get(userEmail);
	}
	
	public String saveUser(User user) {

		if (userMap.containsKey(user.getEmailAddress())) {
			return null;
		} else {
			userMap.put(user.getEmailAddress(), user);
			return user.getEmailAddress();
		}
	}
	
	public void updateUser(User user) {

		
		userMap.put(user.getEmailAddress(), user);

	}
	
	public void deleteUser(String userEmail) {

		userMap.remove(userEmail);

	}
	
	public List<User> findUserByType(UserType userType) {
		List<User> userByType = new ArrayList<>();
		//userMap.values().stream().filter(user -> user.getUserType().toString().equalsIgnoreCase(userType.toString()))
		userMap.values().stream().filter(user -> userType.equals(user.getUserType()))
			.forEach(userByType::add);
		return userByType;
	}
	
	public User findUserByCredential(String emailAddress, String password) {
		User user = userMap.get(emailAddress);
		if (user.getPassword().equals(Digest.digestSHA1(password))) {
			return user;
		} else {
			return null;
		}
	}

}
