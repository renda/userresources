package uk.co.photobox.ppagnoniTest.dao;

import java.util.List;

import uk.co.photobox.ppagnoniTest.domain.model.User;
import uk.co.photobox.ppagnoniTest.domain.type.UserType;
import uk.co.photobox.ppagnoniTest.persistence.UserPersistence;

public class UserDaoImpl implements UserDao {

	private static UserPersistence userPersistence = new UserPersistence();
	
	@Override
	public List<User> findAllUsers() {
		return userPersistence.findAllUser();
	}

	@Override
	public User findUserById(String userEmail) {
		return userPersistence.findUserById(userEmail);
	}

	@Override
	public String saveUser(User user) {
		return userPersistence.saveUser(user);
	}

	@Override
	public void updateUser(User user) {
		userPersistence.updateUser(user);
	}
	
	@Override
	public void deleteUser(String userEmail) {
		userPersistence.deleteUser(userEmail);
	}

	@Override
	public List<User> findUserByType(UserType userType) {
		return userPersistence.findUserByType(userType);
	}

	@Override
	public User findUserByCredential(String emailAddress, String password) {
		return userPersistence.findUserByCredential(emailAddress, password);
	}
}
