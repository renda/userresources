package uk.co.photobox.ppagnoniTest.services;

import java.util.List;

import uk.co.photobox.ppagnoniTest.domain.model.User;
import uk.co.photobox.ppagnoniTest.domain.type.UserType;

public interface UserService {
	
	/**
	 * Get the list of all the users
	 * @return list of users
	 */
	public List<User> findAllUsers();
	
	/**
	 * Get user by email
	 * 
	 * @param userEmail email of the user
	 * @return user or null in case the user doesn't exist
	 */
	public User findUserById(String userEmail);
	
	/**
	 * Create the user
	 * 
	 * @param user user
	 * @return the email address
	 */
	public String saveUser(User user);
	
	/**
	 * Update user
	 * 
	 * @param user user
	 */
	public void updateUser(User user);
	
	/**
	 * Delete user by email
	 * 
	 * @param userEmail email of the user to delete
	 */
	public void deleteUser(String userEmail);
	
	/**
	 * Get users by type (Subscriber, Administrator, SuperUSer)
	 * 
	 * @param userType type of the users
	 * @return List of users
	 */
	public List<User> findUserByType(UserType userType);
	
	/**
	 * Get user by email and password
	 * 
	 * @param emailAddress email of the user
	 * @param password user password
	 * @return user or null in case the user doesn't exist
	 */
	public User findUserByCredential(String emailAddress, String password);
}
