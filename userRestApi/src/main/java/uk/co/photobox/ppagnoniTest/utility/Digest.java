package uk.co.photobox.ppagnoniTest.utility;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.co.photobox.ppagnoniTest.api.UserResources;

public class Digest {

    private static final Logger logger = LoggerFactory.getLogger(UserResources.class);
	
	public static String digestSHA1(String s) {
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("SHA1");
			byte[] messageDigest = md.digest(s.getBytes());
			BigInteger number = new BigInteger(1, messageDigest);
			String hashtext = number.toString(16);
			md.reset();
			return hashtext;
		} catch (NoSuchAlgorithmException e) {
			logger.error("No algorithm for the digest {}", e.getMessage());
			return null;
		}
	}
}
