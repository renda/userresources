package uk.co.photobox.ppagnoniTest.services;

import java.util.List;

import uk.co.photobox.ppagnoniTest.dao.UserDao;
import uk.co.photobox.ppagnoniTest.dao.UserDaoImpl;
import uk.co.photobox.ppagnoniTest.domain.model.User;
import uk.co.photobox.ppagnoniTest.domain.type.UserType;

public class UserServiceImpl implements UserService{

	private UserDao userDao = new UserDaoImpl();
	
	@Override
	public List<User> findAllUsers() {
		return userDao.findAllUsers();
	}

	@Override
	public User findUserById(String emailAddress) {
		return userDao.findUserById(emailAddress);
	}

	@Override
	public String saveUser(User user) {
		return userDao.saveUser(user);
	}

	@Override
	public void updateUser(User user) {
		userDao.updateUser(user);
	}
	
	@Override
	public void deleteUser(String emailAddress) {
		userDao.deleteUser(emailAddress);
	}

	@Override
	public List<User> findUserByType(UserType userType) {
		return userDao.findUserByType(userType);
	}

	@Override
	public User findUserByCredential(String emailAddress, String password) {
		return userDao.findUserByCredential(emailAddress, password);
	}
}
